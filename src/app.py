import streamlit as st
import pandas as pd
import yaml
import time
from typing import Union
from transformers import pipeline



# PATH = "./config.yaml"




def load_yaml_config(file_path):
    with open(file_path, 'r') as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

# this is a comment -don't touch
# another comment 

def sentiment_model(task, model):
    classifier = pipeline(task=task, model=model)
    return classifier


def file_uploader() -> Union[pd.DataFrame, None]:
    uploaded_file = st.file_uploader("Choose a CSV file", type="csv")
    if uploaded_file is not None:
        df = pd.read_csv(uploaded_file, header=None, names=["comments", "sentiment"])
        return df
    else:
        return None


def main():
    st.title("Sentiment Analysis 😃 😕 😃")
    st.write("Upload the opinions you want to analyse 🌡")
    sentiment_dataframe = file_uploader()
    #config = load_yaml_config(PATH)
    task = "sentiment-analysis"
    model = "cardiffnlp/twitter-roberta-base-sentiment-latest"
    if sentiment_dataframe is not None:
        sentiment_classifier = sentiment_model(task=task, model=model)
        with st.spinner('Analysing text ...'):
            time.sleep(10)
            st.info("Sentiment Analysis: ")
            sentiment_dataframe["sentiment"] = sentiment_dataframe["comments"].map(sentiment_classifier)
            sentiment_dataframe["sentiment_label"] = sentiment_dataframe["sentiment"].apply(lambda x: x[0]["label"])
            sentiment_dataframe["sentiment_score"] = sentiment_dataframe["sentiment"].apply(lambda x: x[0]["score"])
            sentiment_dataframe = sentiment_dataframe.drop("sentiment", axis=1)
            st.dataframe(sentiment_dataframe)


if __name__ == "__main__":
    main()



