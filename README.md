# Sentiment Analysis Dashboard

Demo project for the video tutorial: [Building Machine Learning apps with GitLab, Streamlit, and Huggingface](https://go.gitlab.com/ttZu5D). 


### Instructions

1. Create a virtual environment and install the dependencies:

(You'll need Python 3.11 or less for [compatability with the `torchvision` package](https://pypi.org/project/torchvision/))

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

2. Run the Streamlit App `streamlit run src/app.py`

If you prefer, you can build and run the application in Docker

1. Build the docker image `docker build -t sentiment-analysis-app`
2. Run the docker container `docker run -p 8501:8501 sentiment-analysis-app`

To run the tests `pytest test_load_app.py`

For troubleshooting, verify configurations in `config.yaml` and check the Streamlit lots for errors.