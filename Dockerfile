FROM python:3.10-slim-bullseye
LABEL maintainer="William Arias"
COPY .    /app/
WORKDIR /app
RUN pip3 install -r requirements.txt
EXPOSE 8501
ENTRYPOINT ["streamlit", "run"]
CMD ["src/app.py"]

